var ready = true;
var resultsShown = false;
var leftToFetch = 0;
var results = [];
var sizes = [75, 150, 300, 375];
var layers = [1, 2, 4, 8];
var chart;
var graph = false;
var colours = ['rgb(255, 99, 132)', 'rgb(255, 159, 64)', 'rgb(75, 192, 192)', 'rgb(54, 162, 235)', 'rgb(153, 102, 255)', 'rgb(201, 203, 207)'];
$(document).ready(function () {
    setupChart();
    $(".switch").click(function() {
        if (graph) {
            hideGraph();
        }else{
            showGraph();
        }
    })
    $(".test").click(function () {
        // Only submit if it's not currently submitting
        if (!ready) return;
        markAsBusy();
        resetResults();
        if (!resultsShown) {
            resultsShown = true;
            displayResultsSection();
        }
        var input = $(".text-input").val();
        // Start submitting requests for results
        var i = 0;
        for (let layer = 0; layer < layers.length; layer++) {
            for (let size = 0; size < sizes.length; size++) {
                results[i++] = {};
                const value = i - 1;
                classify(input, sizes[size], layers[layer], function (data) {
                    leftToFetch--;
                    if (data.success || !data.success) {
                        results[value] = data;
                        updateResult(data.result, data.layerCount, data.layerSize);
                    }
                    if (leftToFetch == 0) {
                        markAsReady();
                    }
                });
            }
        }
        leftToFetch = results.length;
    })
});

function displayResultsSection() {
    $(".results").fadeIn(100);
}

function markAsReady() {
    ready = true;
    $(".progress-indicator").fadeOut(200);
}

function markAsBusy() {
    // Mark as submitting
    ready = false;
    // Show progress symbol
    $(".progress-indicator").fadeIn(200);
}

function resetResults() {
    // Hide overall results
    $(".results-avg").fadeOut(50);
    $(".results-best").fadeOut(50);
    // Hide per layer results (they'll appear as they're collected)
    $(".layer-result").fadeOut(50);
}

function updateResult(accuracy, layerCount, layerSize) {
    // Update self
    updateProgressBar($(".result-" + layerCount + "-" + layerSize + " .progress-bar"), accuracy);
    $(".result-" + layerCount + "-" + layerSize).fadeIn(100);

    var avg = 0;
    for (var x in results) {
        if (results[x].result) {
            avg += results[x].result;
        }
    }
    avg = avg / (results.length - leftToFetch);
    // Update averages
    updateProgressBar($(".results-avg .progress-bar"), avg);
    $(".results-avg").fadeIn(100);

    if (layerCount == 1 && layerSize == 375) {
        // Best
        updateProgressBar($(".results-best .progress-bar"), accuracy);
        $(".results-best").fadeIn(100);
    }

    // Update chart
    for (var i = 0; i < layers.length; i++) {
        if (layers[i] == layerCount) {
            for (var j = 0; j < sizes.length; j++) {
                if (sizes[j] == layerSize) {
                    chart.data.datasets[i].data[j] = (accuracy * 100.0).toFixed(2);
                }
            }

        }
    }
    chart.update();
}

function updateProgressBar(progressBar, accuracy) {
    var percentage = (accuracy * 100.0).toFixed(2); // 2 decimal places
    var type = percentage >= 50 ? "bg-danger" : "bg-success";
    progressBar.css('width', percentage + '%')
        .attr('aria-valuenow', percentage)
        .text(percentage + '%')
        .removeClass("bg-danger")
        .removeClass("bg-success")
        .addClass(type);
}

function setupChart() {
    var datasets = [];
    for (var i = 0; i < layers.length; i++) {
        datasets[i] = {
            fill: false,
            label: layers[i] + " Layer",
            backgroundColor: colours[i],
            borderColor: colours[i],
            data: []
        }
    }
    chart = new Chart(document.getElementById('chart').getContext('2d'), {
        type: 'line',
        data: {
            labels: sizes,
            datasets: datasets
        },
        options: {
            scales: {
                xAxes: [{
                    scaleLabel: {
                        display: true,
                        labelString: "LSTM Layer Size"
                    }
                }],
                yAxes: [{
                    ticks: {
                        min: 0,
                        max: 100,
                        stepSize: 10
                    },
                    scaleLabel: {
                        display: true,
                        labelString: "Abusive Percentage"
                    }
                }]
            }
        }
    });
}
function showGraph() {
    $("#chart").fadeIn(500);
    $(".no-graph").fadeOut(50);
    graph = true;
    $(".switch").text("Switch to Data View");
}
function hideGraph() {
    $("#chart").fadeOut(50);
    $(".no-graph").fadeIn(500);
    graph = false;
    $(".switch").text("Switch to Graph View");
}