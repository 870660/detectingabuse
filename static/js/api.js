function classify(input, layerSize = 375, layerCount = 1, callback) {
    $.get("/api/classify", {input: input, layerSize: layerSize, layerCount: layerCount}, function (data) {
        callback(data);
    });
}