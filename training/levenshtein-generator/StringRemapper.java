import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class StringRemapper {
    public static void main(String[] args) throws IOException {
        // Load our dictionary
        System.out.println("Start dictionary");
        Map<String, Boolean> map = new HashMap<>();
        int j = 0;
        try (BufferedReader br = new BufferedReader(new FileReader("dictionary.txt"))) {
            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) {
                map.put(sCurrentLine.toLowerCase(), true);
                j++;
                if (j % 10000 == 0) {
                    System.out.println(j);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(dictionary.size());
        System.out.println("Loaded dictionary");
        List<String> tokens = new ArrayList<>(100_000);
        try (BufferedReader br = new BufferedReader(new FileReader("tokens.txt"))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                tokens.add(sCurrentLine);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Loaded tokens");
        System.out.println("Calculating tokens to be remapped");
        LinkedHashMap<String, String> outputValues = new LinkedHashMap<>();
        List<String> toRemap = new ArrayList<>();
        for (String token : tokens) {
            if (!map.containsKey(token.toLowerCase())) {
                String test2 = token.toLowerCase().replaceAll("[^A-Za-z]", "");
                if (map.containsKey(test2)) {
                    outputValues.put(token, test2);
                } else {
                    if (test2.length() > 3) {
                        try {
                            int x = Integer.parseInt(test2);
                        } catch (Exception e) {
                            toRemap.add(token);
                        }
                    }
                }
            }
        }

        System.out.println("Need to remap: " + toRemap.size() + ", Already remapped " + outputValues.size() + ", Total: " + (toRemap.size() + outputValues.size()));
        int i = 0;
        for (String token : toRemap) {
            String lowerCaseToken = token.toLowerCase().replace("'", "");
            int minDist = token.length();
            String minWord = null;
            Optional<SymSpell.suggestItem> output = SymSpell.Lookup(lowerCaseToken, "en", 3).stream().reduce((x, y) -> x.distance < y.distance ? x : y);
            i++;
            if (output.isPresent()) {
                outputValues.put(token, output.get().term);
            }
        }


        System.out.println("Done");
        FileWriter writer = new FileWriter("remap.json");
        new Gson().toJson(outputValues, writer);
        writer.close();
    }
}
