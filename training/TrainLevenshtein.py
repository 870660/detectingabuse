import csv
import os
import gensim
import numpy
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Reshape, Embedding, LSTM
from keras.engine import Input
from keras.preprocessing import text, sequence
from keras.utils import plot_model
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import pandas

# Parameters
lstm_layer_size = 375

# Configuration settings
word_vector_size = 300
max_sequence_length = 200

# Read dataset
data = pandas.read_csv("train.csv")
x = data['comment_text'].apply(str).values
y = data['toxic'].values
print x

# Build vocabulary from dataset
tokens = text.Tokenizer(lower=True, split=" ")
tokens.fit_on_texts(x) # Build dictionary from text
# Read our remapping file (specially created for this dataset)
with open("remap.json", "r") as f:
    data = f.read()

# decoding the JSON to dictionay
d = dict(json.loads(data))
for old, new in d.items():
    # If the word list contains the new word:
    if new.encode('utf-8') in tokens.word_index:
        tokens.word_index[old.encode('utf-8')] = tokens.word_index[new.encode('utf-8')]
    else:
        # Create a new value with the word (so that when tensorflow applies weights it uses the index)
        tokens.word_index[new.encode('utf-8')] = tokens.word_index[old.encode('utf-8')]
x = tokens.texts_to_sequences(x) # Transform x into word indexes

# Build sequences of word indexes
x = sequence.pad_sequences(x, maxlen=max_sequence_length)

# Load Google News Word2Vector model
print('Loading Word2Vector Data')
w2v_data = gensim.models.KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True)
print('Done!')

# Create empty matrix for words
token_size = len(tokens.word_index) + 1 # +1 as index 0 is reserved for sequence padding
word_matrix = numpy.zeros((token_size, word_vector_size))

# Loop through each word and assign the correct vector value
for word, i in tokens.word_index.items():
    if word in w2v_data.vocab:
        word_matrix[i] = w2v_data[word]

# Create the network
model = Sequential()
# Embed word2vector
model.add(Embedding(token_size, word_vector_size, weights=[word_matrix], input_length=max_sequence_length))
# Following lines are duplicated for more / less layers
model.add(LSTM(units=lstm_layer_size, return_sequences=True))
model.add(LSTM(units=lstm_layer_size, return_sequences=True))
model.add(LSTM(units=lstm_layer_size, return_sequences=True))
model.add(LSTM(units=lstm_layer_size))
model.add(Dropout(0.25))
model.add(Dense(1))
model.add(Activation('sigmoid'))
model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

# Save the model so we can convert it to tf.keras format
model.save("temp-model")
new_model = tf.keras.models.load_model("temp-model")

print('Dynamic Google News Word2Vec using TPU')
# Convert to TPU Model
TPU_WORKER = "grpc://" + os.environ["COLAB_TPU_ADDR"]
tf.logging.set_verbosity(tf.logging.INFO)

tpuModel = tf.contrib.tpu.keras_to_tpu_model(new_model, strategy=tf.contrib.tpu.TPUDistributionStrategy(
    tf.contrib.cluster_resolver.TPUClusterResolver(TPU_WORKER)))

# Split into testing + training
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)
# Train the model (change batch_size depending on RAM / GPU RAM available)
tpuModel.fit(x_train, y_train, batch_size=1024, epochs=100, validation_data=(x_test, y_test), shuffle=True)
# Save the output model
tpuModel.save("ltsm-" + str(lstm_layer_size) + "-epoch-" + str(nb_epoch) + ".h5")
tpuModel.summary()