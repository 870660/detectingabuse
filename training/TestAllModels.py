import json
import os
import sys

import tensorflow as tf
from keras.preprocessing import text, sequence
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import pandas

# Note: These models need Tensorflow 1.13.1 to work
# pip install 'tensorflow==1.13.1' --force-reinstall

# Parameters
lstm_layer_size = 375

# Configuration settings
word_vector_size = 300
max_sequence_length = 200


# Read dataset
data = pandas.read_csv("train.csv")
original_x = data['comment_text'].apply(str).values
y = data['toxic'].values
print original_x
# Loop through each model
for filename in os.listdir('.'): # This will test all models from the current directory
    if filename.endswith('.h5'):
        print "Doing " + filename

        # Build vocabulary from dataset
        tokens = text.Tokenizer(lower=True, split=" ")
        tokens.fit_on_texts(original_x)
        # If levenshtein
        if filename.startswith('leven'): #
            # Read our remapping file (specially created for this dataset)
            with open("remap.json", "r") as f:
                data2 = f.read()

            # decoding the JSON to dictionay
            d = dict(json.loads(data2))
            for old, new in d.items():
                # If the word list contains the new word:
                if new.encode('utf-8') in tokens.word_index:
                    tokens.word_index[old.encode('utf-8')] = tokens.word_index[new.encode('utf-8')]
                else:
                    # Create a new value with the word (so that when tensorflow applies weights it uses the index)
                    tokens.word_index[new.encode('utf-8')] = tokens.word_index[old.encode('utf-8')]

        x = tokens.texts_to_sequences(original_x)

        # Build sequences of word indexes
        x = sequence.pad_sequences(x, maxlen=max_sequence_length)
        model = tf.keras.models.load_model(filename)

        # Split data
        x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.25)
        print "Doing summary and prediction"
        orig_stdout = sys.stdout
        f = open(filename + '-out.txt', 'w') # Output log saved to -out.txt file
        sys.stdout = f
        model.summary()
        # Print results

        y_pred = model.predict(x_test)
        y_pred = (y_pred >= 0.5)

        for x_1 in range(len(y_pred)):
            y_1 = y_pred[x_1][0]
            if y_1 != (y[x_1] >= 0.5):
                if not y_1:
                    print('False negative: ' + data['comment_text'][x_1])

        for x_1 in range(len(y_pred)):
            y_1 = y_pred[x_1][0]
            if y_1 != (y[x_1] >= 0.5):
                if y_1:
                    print('False positive: ' + data['comment_text'][x_1])

        print(confusion_matrix(y_test, y_pred))
        # Confusion matrix from all data
        sys.stdout = orig_stdout
        f.close()
        print "Done!"
