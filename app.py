import os

import pandas as pandas
import tensorflow as tf
from flask import Flask, render_template, jsonify, request, make_response
from keras.preprocessing import text, sequence

app = Flask(__name__)
tk = text.Tokenizer(lower=True, split=" ")
networks = dict()
graphs = dict()
sessions = dict()


def pre_load():
    # Load original training set to generate word indexes
    train_data = pandas.read_csv("train.csv")
    fitting_data = train_data['comment_text'].apply(str).values
    tk.fit_on_texts(fitting_data)
    # Load networks
    for filename in os.listdir('networks'):
        graphs[filename] = tf.Graph()  # https://github.com/keras-team/keras/issues/8538#issuecomment-358720718
        with graphs[filename].as_default():
            sessions[filename] = tf.Session()
            with sessions[filename].as_default():
                networks[filename] = tf.keras.models.load_model("networks/" + filename)


def predict(networkFile, inputText):
    # Generate tokens for sequence
    x = [inputText]
    x = tk.texts_to_sequences(x)
    x = sequence.pad_sequences(x, maxlen=200)
    # Load model
    with graphs[networkFile].as_default():
        with sessions[networkFile].as_default():
            model = networks[networkFile]
            # Predict
            y_pred = model.predict(x)
    return y_pred[0][0]


@app.route('/')
def index():
    return render_template('home.html')


@app.route('/embed')
def embed():
    return render_template('embed.html')


@app.route('/bulk', methods=['GET'])
def batch():
    return render_template('batch.html')


@app.route('/bulk', methods=['POST'])
def batch_submit():
    the_file = request.files['file']
    data = pandas.read_csv(the_file, header=None)
    # Change first header to Data
    labels = list(data.columns.values)
    labels[0] = "input_text"
    data.columns = labels
    # Generate tokens
    x = tk.texts_to_sequences(data["input_text"].apply(str).values)
    x = sequence.pad_sequences(x, maxlen=200)
    # Run through each neural network
    for name in sorted(networks.keys()):
        with graphs[name].as_default():
            with sessions[name].as_default():
                # Predict
                y_pred = networks[name].predict(x)
                # Save to column
                data[name] = y_pred

    # Save the data
    resp = make_response(data.to_csv(index=False))
    resp.headers["Content-Type"] = "text/csv"
    resp.headers["Content-Disposition"] = "attachment; filename=results.csv"
    return resp


@app.route('/api/classify')
def classify():
    input = request.args.get('input', default='', type=str)
    layer_count = request.args.get('layerCount', default=1, type=int)
    layer_size = request.args.get('layerSize', default=375, type=int)
    file_count = 0
    if layer_count == 2:
        file_count = 1
    if layer_count == 4:
        file_count = 2
    if layer_count == 8:
        file_count = 3
    return jsonify(
        {'result': float(
            predict("ltsm-" + str(file_count) + "-" + str(layer_size) + "-epoch-100.h5",
                    input)),
            'success': True, 'input': input, 'layerCount': layer_count, 'layerSize': layer_size})


if __name__ == '__main__':
    pre_load()
    app.run()
