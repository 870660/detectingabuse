### Detecting Abusive Chat Online using Machine Learning
The following repository contains code developed in the project as well as trained models so that the results can be replicated.

Requirements:
- 4GB RAM
- Python 2.7
- Tensorflow 1.13.1 (This specific version is required to load the models) `pip install 'tensorflow==1.13.1' --force-reinstall`
- Keras `pip install keras`
- Flask `pip install flask`
- Pandas `pip install pandas`

Running:
1. Run `python app.py`
2. Wait for the software to load the networks (may take a few minutes)
3. Navigate to `http://localhost:5000` for the interface.

Sources:

- This repository contains a dataset from Google Detox project - https://meta.wikimedia.org/wiki/Research:Detox
